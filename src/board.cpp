#include "board.hpp"

#include <gamechannel/protoversion.hpp>
#include <xayautil/hash.hpp>
#include <xayautil/random.hpp>
#include <xayautil/uint256.hpp>

#include <glog/logging.h>

#include <sstream>

namespace chess
{
  namespace
  {
    bool CheckHashValue(const xaya::uint256 &actual, const std::string &expected)
    {
      if (expected.size() != xaya::uint256::NUM_BYTES)
      {
        LOG(WARNING) << "Commited hash has wrong size " << expected.size();
        return false;
      }
      LOG(INFO) << "Checking hash value " << actual.GetBinaryString() << " vs "
                << expected;
      return actual.GetBinaryString() == expected;
    }

    bool CheckSoldierProps(const proto::Soldier &soldier)
    {
      if (soldier.coord().x() < 0 || soldier.coord().x() > 8)
      {
        LOG(WARNING) << "Soldier x coordinate out of range";
        return false;
      }
      if (soldier.coord().y() < 0 || soldier.coord().y() > 9)
      {
        LOG(WARNING) << "Soldier y coordinate out of range";
        return false;
      }
      if (soldier.class_() < proto::Soldier::Class::Soldier_Class_Base || soldier.class_() > proto::Soldier::Class::Soldier_Class_Base)
      {
        LOG(WARNING) << "Soldier class out of range";
        return false;
      }
      return true;
    }

    int DetermineRoundWinner(const proto::BoardState &state)
    {
      CHECK_EQ(state.soldierlist(0).soldiers_size(), state.soldierlist(1).soldiers_size());
      return 0;
    }
  }

  bool ChessBoardState::IsValid() const
  {
    /* Single-participant states are only ever created for waiting for the second player to join */
    if (GetMetadata().participants_size() == 1)
      return true;

    CHECK_EQ(GetMetadata().participants_size(), 2);

    /* If phase invalid, the state is invalid */
    const Phase phase = GetPhase();
    if (phase == Phase::INVALID)
      return false;

    /* Unless the game is finished, we should have a turn set.  */
    const proto::BoardState &pb = GetState();
    if (!pb.has_turn() || phase == Phase::FINISHED)
      return !pb.has_turn() && phase == Phase::FINISHED;

    /* Since we have two players, turn should be zero or one.  */
    const int turn = pb.turn();
    if (turn < 0 || turn > 1)
      return false;

    switch (phase)
    {
    case Phase::FIRST_COMMITMENT:
    case Phase::FIRST_REVEAL:
      if (turn != 0)
        return false;
      break;
    case Phase::SECOND_COMMITMENT:
    case Phase::SECOND_REVEAL:
      if (turn != 1)
        return false;
      break;
    default:
      LOG(FATAL) << "Unexpected phase " << static_cast<int>(phase);
      break;
    }

    return true;
  }

  ChessBoardState::Phase ChessBoardState::GetPhase() const
  {
    const proto::BoardState &pb = GetState();

    if (pb.has_winner())
      return Phase::FINISHED;
    switch (pb.commitment_hash_size())
    {
    case 0:
      return Phase::FIRST_COMMITMENT;
    case 1:
      return Phase::SECOND_COMMITMENT;
    case 2:
      if (!pb.commitment_hash(0).empty())
        return Phase::FIRST_REVEAL;
      else
        return Phase::SECOND_REVEAL;
    default:
      return Phase::INVALID;
    }
  }

  int ChessBoardState::WhoseTurn() const
  {
    if (GetMetadata().participants_size() == 1)
      return xaya::ParsedBoardState::NO_TURN;

    if (!GetState().has_turn())
      return xaya::ParsedBoardState::NO_TURN;

    const int res = GetState().turn();
    CHECK_GE(res, 0);
    CHECK_LE(res, 1);

    return res;
  }

  Json::Value ChessBoardState::ToJson() const
  {
    Json::Value res = BaseProtoBoardState::ToJson();

    if (GetMetadata().participants_size() == 1)
    {
      res["phase"] = "single participant";
      return res;
    }

    const Phase phase = GetPhase();
    switch (phase)
    {
    case Phase::FIRST_COMMITMENT:
      res["phase"] = "first commitment";
      break;
    case Phase::SECOND_COMMITMENT:
      res["phase"] = "second commitment";
      break;
    case Phase::FIRST_REVEAL:
      res["phase"] = "first reveal";
      break;
    case Phase::SECOND_REVEAL:
      res["phase"] = "second reveal";
      break;
    case Phase::FINISHED:
      res["phase"] = "finished";
      break;
    default:
      LOG(FATAL) << "Invalid phase: " << static_cast<int>(phase);
    }

    const proto::BoardState &pb = GetState();
    if (pb.has_winner())
      res["winner"] = pb.winner();

    if (pb.soldierlist_size() > 0)
    {
      CHECK_EQ(pb.soldierlist_size(), 2);
      Json::Value soldierList(Json::arrayValue);
      for (const proto::SoldierList &sl : pb.soldierlist())
      {
        Json::Value soldiers(Json::arrayValue);
        for (auto &s : sl.soldiers())
        {
          Json::Value soldier(Json::objectValue);
          soldier["x"] = s.coord().x();
          soldier["y"] = s.coord().y();
          soldier["class"] = s.class_();
          soldiers.append(soldier);
        }
        soldierList.append(soldiers);
      }
      res["soldierList"] = soldierList;
    }

    if(pb.wincount_size() > 0)
    {
      CHECK_EQ(pb.wincount_size(), 2);
      Json::Value winCount(Json::arrayValue);
      for (const auto &wc : pb.wincount())
      {
        winCount.append(wc);
      }
      res["winCount"] = winCount;
    }

    res["round"] = pb.round();
    res["step"] = pb.step();

    return res;
  }

  unsigned ChessBoardState::TurnCount() const
  {
    if (GetMetadata().participants_size() == 1)
      return 0;

    const proto::BoardState &pb = GetState();
    const Phase phase = GetPhase();
    const int prevTurns = pb.step() * 4 + pb.round() * 12;
    switch (phase)
    {
    case Phase::FINISHED:
    case Phase::FIRST_COMMITMENT:
      return 1 + prevTurns;
    case Phase::SECOND_COMMITMENT:
      return 2 + prevTurns;
    case Phase::FIRST_REVEAL:
      return 3 + prevTurns;
    case Phase::SECOND_REVEAL:
      return 4 + prevTurns;
    default:
      LOG(FATAL) << "Invalid phase: " << static_cast<int>(phase);
      break;
    }
  }

  bool ChessBoardState::ApplySetPieceCommitment(const proto::SetPieceCommitmentMove &mv, Phase phase, proto::BoardState &newState)
  {
    switch (phase)
    {
    case Phase::FIRST_COMMITMENT:
      CHECK_EQ(newState.commitment_hash_size(), 0);
      break;
    case Phase::SECOND_COMMITMENT:
      CHECK_EQ(newState.commitment_hash_size(), 1);
      break;
    default:
      LOG(WARNING)
          << "Invalid phase for set piece commitment: " << static_cast<int>(phase);
      return false;
    }

    if (mv.hash().size() != xaya::uint256::NUM_BYTES)
    {
      LOG(WARNING) << "First piece commitment has wrong hash size";
      return false;
    }
    const int turn = newState.turn();
    const int otherTurn = 1 - turn;
    CHECK_GE(otherTurn, 0);
    CHECK_LE(otherTurn, 1);
    newState.add_commitment_hash(mv.hash());
    newState.set_turn(otherTurn);
    return true;
  }

  bool ChessBoardState::ApplySetPieceReveal(const proto::SetPieceRevealMove &mv, Phase phase, proto::BoardState &newState)
  {
    switch (phase)
    {
    case Phase::FIRST_REVEAL:
    case Phase::SECOND_REVEAL:
      break;
    default:
      LOG(WARNING) << "Invalid phase for piece reveal: " << static_cast<int>(phase);
      return false;
    }
    const int turn = newState.turn();
    const int otherTurn = 1 - turn;
    CHECK_GE(otherTurn, 0);
    CHECK_LE(otherTurn, 1);

    if (!mv.has_salt())
    {
      LOG(WARNING) << "Piece reveal has no salt";
      return false;
    }

    xaya::SHA256 hasher;
    xaya::uint256 salt;
    salt.FromHex(mv.salt());
    hasher << mv.soldier().SerializeAsString() << salt;
    if (!CheckHashValue(hasher.Finalise(), newState.commitment_hash(turn)))
    {
      LOG(WARNING) << "Piece reveal has wrong hash";
      return false;
    }

    if (!CheckSoldierProps(mv.soldier()))
    {
      LOG(WARNING) << "Piece reveal has invalid soldier props";
      return false;
    }

    newState.mutable_soldierlist(turn)->add_soldiers()->CopyFrom(mv.soldier());

    newState.set_turn(otherTurn);

    newState.set_commitment_hash(turn, "");

    if (phase == Phase::SECOND_REVEAL)
    {
      newState.set_step(newState.step() + 1);
      newState.clear_commitment_hash();

      CHECK_EQ(newState.soldierlist(0).soldiers_size(), newState.soldierlist(1).soldiers_size());
      if(newState.soldierlist(0).soldiers_size() == 3)
      {
        const int roundWinner = DetermineRoundWinner(newState);
        CHECK_GE(roundWinner, 0);
        CHECK_LE(roundWinner, 1);
        newState.set_wincount(roundWinner, newState.wincount(roundWinner) + 1);

        newState.set_step(0);
        newState.set_round(newState.round() + 1);

        if(newState.wincount(roundWinner) >= 3)
          newState.set_winner(roundWinner);


        // Clear soldier lists
        for (int i = 0; i < 2; i++)
          newState.mutable_soldierlist(i)->clear_soldiers();
      }

      if(newState.has_winner())
        newState.clear_turn();
    }
    return true;
  }

  bool ChessBoardState::ApplyMoveProto(const proto::BoardMove &mv, proto::BoardState &newState) const
  {
    const proto::BoardState &pb = GetState();
    newState = pb;

    const int turn = WhoseTurn();
    CHECK_NE(turn, xaya::ParsedBoardState::NO_TURN);

    const Phase phase = GetPhase();
    switch (mv.move_case())
    {
    case proto::BoardMove::kPositionCommitment:
      return ApplySetPieceCommitment(mv.position_commitment(), phase, newState);
    case proto::BoardMove::kPositionReveal:
      return ApplySetPieceReveal(mv.position_reveal(), phase, newState);
    default:
    case proto::BoardMove::MOVE_NOT_SET:
      LOG(WARNING) << "Move does not specify any one-of case";
      return false;
    }

    LOG(FATAL) << "Unexpected move case: " << mv.move_case();
    return false;
  }

  xaya::ChannelProtoVersion ChessBoardRules::GetProtoVersion(const xaya::proto::ChannelMetadata &meta) const
  {
    return xaya::ChannelProtoVersion::ORIGINAL;
  }

  proto::BoardState InitialBoardState()
  {
    proto::BoardState res;
    res.set_round(0);
    res.set_step(0);
    res.set_turn(0);
    for (int i = 0; i < 2; i++)
    {
      res.add_soldierlist();
      res.add_wincount(0);
    }
    return res;
  }
}