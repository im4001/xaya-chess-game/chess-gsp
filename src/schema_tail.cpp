  )";

  } // anonymous namespace

  void
  SetupChessSchema(xaya::SQLiteDatabase &db)
  {
    LOG(INFO) << "Setting up the database schema for xayachess...";
    db.Execute(SCHEMA_SQL);
  }

} // namespace chess
