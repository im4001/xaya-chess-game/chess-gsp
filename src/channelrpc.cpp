// Copyright (C) 2019-2022 The Xaya developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "channelrpc.hpp"

#include <glog/logging.h>

namespace chess
{

  void
  ChessChannelRpcServer::stop()
  {
    LOG(INFO) << "RPC method called: stop";
    daemon.RequestStop();
  }

  Json::Value
  ChessChannelRpcServer::getcurrentstate()
  {
    LOG(INFO) << "RPC method called: getcurrentstate";
    Json::Value state;
    {
      auto cm = daemon.GetChannelManager().Read();
      state = cm->ToJson();
      /* We need to release the lock here again, since ExtendStateJson uses
         its own lock on the channel manager.  */
    }
    return state;
  }

  Json::Value
  ChessChannelRpcServer::waitforchange(const int knownVersion)
  {
    LOG(INFO) << "RPC method called: waitforchange " << knownVersion;
    Json::Value state = daemon.GetChannelManager().WaitForChange(knownVersion);
    return state;
  }

  namespace
  {

    /**
     * Tries to parse a string into a position and validates it.  Returns true
     * and initialises grid if it is valid, and false otherwise.
     */
    bool ParseAndValidatePosition(const std::string &str, proto::Soldier &s)
    {
      Json::Value soldier;
      if (!Json::Reader().parse(str, soldier))
      {
        LOG(ERROR) << "Invalid soldier string given";
        return false;
      }

      /* TODO: Check that soldier placement is valid */

      s.set_class_(proto::Soldier_Class(soldier["class"].asUInt()));
      s.mutable_coord()->set_x(soldier["coord"]["x"].asUInt());
      s.mutable_coord()->set_y(soldier["coord"]["y"].asUInt());

      return true;
    }

  } // anonymous namespace

  void
  ChessChannelRpcServer::setposition(const std::string &str)
  {
    LOG(INFO) << "RPC method called: setposition\n"
              << str;

    if (channel.IsCommitmentSet())
    {
      LOG(ERROR) << "Already set a position";
      return;
    }

    proto::Soldier s;
    if (!ParseAndValidatePosition(str, s))
      return;

    /* The lock on the channel manager also protects our direct access
       to the open channel (for setting the position).  */
    auto cm = daemon.GetChannelManager().Access();
    channel.SetCommitment(s);
    cm->TriggerAutoMoves();
  }

  bool
  ChessChannelRpcServer::validateposition(const std::string &str)
  {
    LOG(INFO) << "RPC method called: validateposition\n"
              << str;

    proto::Soldier s;
    return ParseAndValidatePosition(str, s);
  }

  // void
  // ChessChannelRpcServer::revealposition()
  // {
  //   LOG(INFO) << "RPC method called: revealposition";

  //   if (channel.IsCommitmentSet())
  //   {
  //     auto cm = daemon.GetChannelManager().Access();
  //     ProcessLocalMove(*cm, channel.GetPositionRevealMove());
  //   }
  //   else
  //     LOG(ERROR) << "Cannot reveal position if it is not set yet";
  // }

  std::string
  ChessChannelRpcServer::filedispute()
  {
    LOG(INFO) << "RPC method called: filedispute";
    auto cm = daemon.GetChannelManager().Access();

    /* If the winner is already known, we can't file an actual dispute, but
       instead we put the state on chain (with a resolution move) which will
       result in closure of the channel.  */
    const auto *state = cm->GetBoardState<ChessBoardState>();
    const bool hasWinner = (state != nullptr && state->GetState().has_winner());

    const xaya::uint256 txid = hasWinner
                                   ? cm->PutStateOnChain()
                                   : cm->FileDispute();

    if (txid.IsNull())
      return "";

    return txid.ToHex();
  }

  void
  ChessChannelRpcServer::ProcessLocalMove(xaya::ChannelManager &cm,
                                          const proto::BoardMove &mv)
  {
    xaya::BoardMove serialised;
    CHECK(mv.SerializeToString(&serialised));
    cm.ProcessLocalMove(serialised);
  }

} // namespace chess
