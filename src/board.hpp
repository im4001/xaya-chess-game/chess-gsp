#pragma once

#include "proto/boardmove.pb.h"
#include "proto/boardstate.pb.h"

#include <gamechannel/protoboard.hpp>
#include <gamechannel/proto/metadata.pb.h>

#include <json/json.h>
#include <string> 

namespace chess
{
  using BaseProtoBoardState = xaya::ProtoBoardState<proto::BoardState, proto::BoardMove>;

  class ChessBoardState : public BaseProtoBoardState
  {
  private:
    enum class Phase
    {
      /* The proto is inconsistent and no phase can be determined */
      INVALID,

      FIRST_COMMITMENT,

      SECOND_COMMITMENT,

      FIRST_REVEAL,

      SECOND_REVEAL,

      /* The game is finished and the winner determined */
      FINISHED
    };

    Phase GetPhase() const;

    static bool ApplySetPieceCommitment(const proto::SetPieceCommitmentMove &mv, Phase phase, proto::BoardState &newState);

    static bool ApplySetPieceReveal(const proto::SetPieceRevealMove &mv, Phase phase, proto::BoardState &newState);

    friend class ChessChannel;

    friend class BoardTests;

  protected:
    bool ApplyMoveProto(const proto::BoardMove &mv, proto::BoardState &newState) const override;

  public:
    using BaseProtoBoardState::BaseProtoBoardState;

    bool IsValid() const override;
    int WhoseTurn() const override;
    unsigned TurnCount() const override;

    Json::Value ToJson() const override;
  };

  class ChessBoardRules : public xaya::ProtoBoardRules<ChessBoardState>
  {
  public:
    xaya::ChannelProtoVersion GetProtoVersion(const xaya::proto::ChannelMetadata &meta) const override;
  };

  proto::BoardState InitialBoardState();

  /**
   * This takes fields of a Soldier piece and
   * returns it in a deterministic way for hashing
   */
  std::string BlobPieceMove(proto::Soldier &soldier);
}