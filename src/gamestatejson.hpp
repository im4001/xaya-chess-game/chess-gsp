#pragma once

#include "board.hpp"

#include <xayagame/sqlitestorage.hpp>

#include <json/json.h>

namespace chess
{
  /**
   * Helper class that allows extracting game-state data as JSON from the
   * current Xayaships global state.
   */
  class GameStateJson
  {

  private:
    /** The underlying database instance.  */
    const xaya::SQLiteDatabase &db;

    /** Our board rules.  */
    const ChessBoardRules &rules;

  public:
    GameStateJson(const xaya::SQLiteDatabase &d, const ChessBoardRules &r)
        : db(d), rules(r)
    {
    }

    GameStateJson() = delete;
    GameStateJson(const GameStateJson &) = delete;
    void operator=(const GameStateJson &) = delete;

    /**
     * Extracts the full current state as JSON.
     */
    Json::Value GetFullJson() const;
  };
}