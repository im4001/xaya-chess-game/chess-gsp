  -- Stats about won/lost games by Xaya name.
  CREATE TABLE IF NOT EXISTS game_stats (

  -- The p/ name for an entry
  name TEXT NOT NULL PRIMARY KEY,

  -- The number of wins.
  wins INTEGER NOT NULL DEFAULT 0,

  -- The number of losses.
  losses INTEGER NOT NULL DEFAULT 0
  );

  -- Extra data about open channels
  CREATE TABLE IF NOT EXISTS channel_extradata (

  -- The channel ID.
  id BLOB PRIMARY KEY,

  -- The height at which the channel was opened.
  createdheight INTEGER NOT NULL,

  -- Number of participants in the channel.
  participants INTEGER NOT NULL
  );

  CREATE INDEX IF NOT EXISTS channel_extradata_by_height_and_participants
  ON channel_extradata (createdheight, participants);