#!/usr/bin/env python3
# Copyright (C) 2019-2022 The Xaya developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

"""
Tests a full channel game including the channel daemons.  There are no edge
cases tested, though (e.g. reorgs), as that is deferred to other
more specialised integration tests.
"""

from chesstest import ChessTest
import json
import random


class FullGameTest (ChessTest):

  def run (self):
    self.generate (110)

    # Create a test channel with two participants (but with the join
    # not yet confirmed on chain).
    self.mainLogger.info ("Creating test channel...")
    addr = [self.newSigningAddress () for _ in range (2)]
    self.sendMove ("alice", {"c": {
      "addr": addr[0],
    }})
    self.generate (1)
    [channelId] = self.getChannelIds ()
    self.sendMove ("bob", {"j": {
      "id": channelId,
      "addr": addr[1],
    }})
    # Start up the two channel daemons.
    self.mainLogger.info ("Starting channel daemons...")
    with self.runChannelDaemon (channelId, "alice", addr[0]) as alice, \
         self.runChannelDaemon (channelId, "bob", addr[1]) as bob:

      daemons = [alice, bob]
      state = self.getSyncedChannelState (daemons)
      self.assertEqual (state["current"]["state"]["parsed"]["phase"],
                        "single participant")
      self.mainLogger.info ("Channel daemons started.")

      # Mine a block for the join to be confirmed.
      self.generate(1)
      state = self.getSyncedChannelState (daemons)
      self.mainLogger.info (json.dumps(state, indent=2))
      
      for i in range (3):
        while(state["current"]["state"]["parsed"]["round"] == i):
          phase, state = self.waitForPhase(daemons, ["first commitment", "second commitment"])
          turn = state["current"]["state"]["whoseturn"]
          soldier = {
            "class": 1, 
            "coord": {
              "x": random.randrange(0, 8), 
              "y": random.randrange(0, 8)
            }
          }
          daemons[turn].rpc._notify.setposition(json.dumps(soldier))
          state = self.getSyncedChannelState (daemons)
          self.mainLogger.info (json.dumps(state, indent=2))
        

      self.mainLogger.info ("Verifying result of channel game...")
      state = self.getSyncedChannelState (daemons)["current"]["state"]["parsed"]
      self.assertEqual (state["phase"], "finished")
      self.assertEqual (state["winner"], 0)

      self.expectPendingMoves("bob", ["l"])
      self.generate(1)
      self.expectGameState({
        "channels": {},
        "gamestats":
        {
          "alice": {"won": 1, "lost": 0},
          "bob": {"won": 0, "lost": 1},
        }
      })
      state = self.getSyncedChannelState (daemons)
      self.assertEqual(state["existsonchain"], False)


      





if __name__ == "__main__":
  FullGameTest ().main ()
