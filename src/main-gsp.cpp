#include "config.h"
#include "logic.hpp"

#include <gamechannel/gsprpc.hpp>
#include <xayagame/defaultmain.hpp>

#include <gflags/gflags.h>
#include <glog/logging.h>

#include <google/protobuf/stubs/common.h>

#include <cstdlib>
#include <iostream>

namespace
{

  DEFINE_string(xaya_rpc_url, "",
                "URL at which Xaya Core's JSON-RPC interface is available");
  DEFINE_int32(xaya_rpc_protocol, 1,
               "JSON-RPC version for connecting to Xaya Core");
  DEFINE_bool(xaya_rpc_wait, false,
              "whether to wait on startup for Xaya Core to be available");

  DEFINE_int32(game_rpc_port, 0,
               "the port at which the game daemon's JSON-RPC server will be"
               " started (if non-zero)");
  DEFINE_bool(game_rpc_listen_locally, true,
              "whether the game daemon's JSON-RPC server should listen locally");

  DEFINE_int32(enable_pruning, -1,
               "if non-negative (including zero), enable pruning of old undo"
               " data and keep as many blocks as specified by the value");

  DEFINE_string(datadir, "",
                "base data directory for game data (will be extended by the"
                " game ID and chain)");

  DEFINE_bool(pending_moves, true,
              "whether or not pending moves should be tracked");

}

int main(int argc, char **argv)
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  google::InitGoogleLogging(argv[0]);

  gflags::SetUsageMessage("Run Xaya Chess Game Daemon");
  gflags::SetVersionString("1.0");
  gflags::ParseCommandLineFlags(&argc, &argv, true);

  if (FLAGS_xaya_rpc_url.empty())
  {
    std::cerr << "Error: --xaya_rpc_url must be set" << std::endl;
    return EXIT_FAILURE;
  }

  if (FLAGS_datadir.empty())
  {
    std::cerr << "Error: --datadir must be specified" << std::endl;
    return EXIT_FAILURE;
  }

  xaya::GameDaemonConfiguration config;
  config.XayaRpcUrl = FLAGS_xaya_rpc_url;
  if (FLAGS_game_rpc_port != 0)
  {
    config.GameRpcServer = xaya::RpcServerType::HTTP;
    config.GameRpcPort = FLAGS_game_rpc_port;
  }
  config.EnablePruning = FLAGS_enable_pruning;
  config.DataDirectory = FLAGS_datadir;

  chess::ChessLogic rules;
  xaya::ChannelGspInstanceFactory instanceFact(rules);
  config.InstanceFactory = &instanceFact;

  chess::ChessPending pending(rules);
  if (FLAGS_pending_moves)
    config.PendingMoves = &pending;

  xaya::SQLiteMain(config, "xaya-chess", rules);
  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}