// Copyright (C) 2019 The Xaya developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#pragma once

#include "board.hpp"

#include "proto/boardmove.pb.h"

#include <gamechannel/boardrules.hpp>
#include <gamechannel/openchannel.hpp>
#include <gamechannel/movesender.hpp>
#include <gamechannel/proto/stateproof.pb.h>
#include <xayautil/cryptorand.hpp>
#include <xayautil/uint256.hpp>

#include <json/json.h>

#include <cstdint>
#include <string>
#include <vector>

namespace chess
{

  /**
   * Chess-specific data and logic for an open channel the player is involved
   * in.  This mostly takes care of the various commit-reveal schemes.
   */
  class ChessChannel : public xaya::OpenChannel
  {

  private:
    /** The player name who is running this channel daemon.  */
    const std::string playerName;

    /** Generator for random salt values.  */
    xaya::CryptoRand rnd;

    /** The position of the set soldier pieces */
    std::vector<proto::Soldier> soldiers;

    proto::Soldier commitment;

    /** Salt for the move committment hash.  */
    xaya::uint256 commitmentSalt;

    /**
     * If this channel corresponds to the first player, then we save the
     * seed for determining the initial player here.
     */
    // xaya::uint256 seed0;

    /**
     * Set to the txid of the submitted "close by loss declaration" move,
     * if we sent one already.  Otherwise null.
     */
    xaya::uint256 txidClose;

    /**
     * Returns the index that the current player has for the given state.
     */
    int GetPlayerIndex(const ChessBoardState &state) const;

    /**
     * Tries to do a position-commitment move (either for the first or second
     * player) as automove.  Returns true if possible (i.e. we have a position).
     */
    bool AutoCommitment(proto::BoardMove &mv);

    /**
     * Real implementation of MaybeAutoMove, for which the conversion to
     * ShipsBoardState and between the proto and BoardMove is taken care of.
     */
    bool InternalAutoMove(const ChessBoardState &state, proto::BoardMove &mv);

  public:
    explicit ChessChannel(const std::string &nm);

    ChessChannel(const ChessChannel &) = delete;
    void operator=(const ChessChannel &) = delete;

    Json::Value ResolutionMove(const xaya::uint256 &channelId,
                               const xaya::proto::StateProof &p) const override;
    Json::Value DisputeMove(const xaya::uint256 &channelId,
                            const xaya::proto::StateProof &p) const override;

    bool MaybeAutoMove(const xaya::ParsedBoardState &state,
                       xaya::BoardMove &mv) override;
    void MaybeOnChainMove(const xaya::ParsedBoardState &state,
                          xaya::MoveSender &sender) override;

    bool IsCommitmentSet() const;

    /**
     * Returns the player's position (must be set for this to be valid).
     */
    const std::vector<proto::Soldier> &GetSoldiers() const;

    /**
     * Sets the player's position from the given Grid if it is valid.
     * Must not be called if IsPositionSet() is already true.
     */
    void SetCommitment(const proto::Soldier &s);

    void ClearCommitment();
  };

} // namespace ships
