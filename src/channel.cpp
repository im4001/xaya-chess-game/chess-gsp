// Copyright (C) 2019-2021 The Xaya developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "channel.hpp"

#include <gamechannel/proto/signatures.pb.h>
#include <gamechannel/protoutils.hpp>
#include <gamechannel/signatures.hpp>
#include <xayautil/base64.hpp>
#include <xayautil/hash.hpp>

namespace chess
{

  ChessChannel::ChessChannel(const std::string &nm)
      : playerName(nm)
  {
    txidClose.SetNull();
  }

  bool ChessChannel::IsCommitmentSet() const
  {
    return commitment.class_() != proto::Soldier::None;
  }

  const std::vector<proto::Soldier> &ChessChannel::GetSoldiers() const
  {
    return soldiers;
  }

  void ChessChannel::SetCommitment(const proto::Soldier &s)
  {
    CHECK(!IsCommitmentSet());

    commitment = s;
    commitmentSalt = rnd.Get<xaya::uint256>();
    LOG(INFO)
        << "Stored commitment class:" << commitment.class_()
        << " at:" << commitment.coord().x() << "," << commitment.coord().y()
        << " and generated salt: " << commitmentSalt.ToHex();

    CHECK(IsCommitmentSet());
  }

  void ChessChannel::ClearCommitment()
  {
    commitment.Clear();
    commitmentSalt.SetNull();
  }

  int ChessChannel::GetPlayerIndex(const ChessBoardState &state) const
  {
    const auto &meta = state.GetMetadata();

    int res = -1;
    for (int i = 0; i < meta.participants_size(); ++i)
      if (meta.participants(i).name() == playerName)
      {
        CHECK_EQ(res, -1);
        res = i;
      }

    CHECK_GE(res, 0);
    CHECK_LE(res, 1);
    return res;
  }

  bool
  ChessChannel::AutoCommitment(proto::BoardMove &mv)
  {
    if (!IsCommitmentSet())
      return false;

    xaya::SHA256 hasher;
    hasher << commitment.SerializeAsString() << commitmentSalt;
    const std::string hashStr = hasher.Finalise().GetBinaryString();

    mv.mutable_position_commitment()->set_hash(hashStr);
    return true;
  }

  bool
  ChessChannel::InternalAutoMove(const ChessBoardState &state,
                                 proto::BoardMove &mv)
  {
    const auto &pb = state.GetState();

    const int index = GetPlayerIndex(state);
    CHECK_EQ(index, pb.turn());

    const auto phase = state.GetPhase();
    switch (phase)
    {
    case ChessBoardState::Phase::FIRST_COMMITMENT:
    case ChessBoardState::Phase::SECOND_COMMITMENT:
    {

      if (!IsCommitmentSet())
        return false;

      xaya::SHA256 hasher;
      // hasher << commitment.SerializeAsString() << commitmentSalt;
      hasher << commitment.SerializeAsString() << commitmentSalt;
      const std::string hashStr = hasher.Finalise().GetBinaryString();
      LOG(INFO) << "Using commitment string: " << commitment.SerializeAsString();
      LOG(INFO) << "Using commitment salt: " << commitmentSalt.ToHex();
      LOG(INFO) << "Generated commitment hash: " << hashStr;
      mv.mutable_position_commitment()->set_hash(hashStr);
      return true;
    }

    case ChessBoardState::Phase::FIRST_REVEAL:
    case ChessBoardState::Phase::SECOND_REVEAL:
    {
      mv.mutable_position_reveal()->set_salt(commitmentSalt.ToHex());
      mv.mutable_position_reveal()->mutable_soldier()->CopyFrom(commitment);
      ClearCommitment();
      return true;
    }

    case ChessBoardState::Phase::FINISHED:
    default:
      LOG(FATAL)
          << "Invalid phase for auto move: " << static_cast<int>(phase);
    }
  }

  namespace
  {

    Json::Value
    DisputeResolutionMove(const std::string &type,
                          const xaya::uint256 &channelId,
                          const xaya::proto::StateProof &p)
    {
      Json::Value data(Json::objectValue);
      data["id"] = channelId.ToHex();
      data["state"] = xaya::ProtoToBase64(p);

      Json::Value res(Json::objectValue);
      res[type] = data;

      return res;
    }

  } // anonymous namespace

  Json::Value
  ChessChannel::ResolutionMove(const xaya::uint256 &channelId,
                               const xaya::proto::StateProof &p) const
  {
    return DisputeResolutionMove("r", channelId, p);
  }

  Json::Value
  ChessChannel::DisputeMove(const xaya::uint256 &channelId,
                            const xaya::proto::StateProof &p) const
  {
    return DisputeResolutionMove("d", channelId, p);
  }

  bool
  ChessChannel::MaybeAutoMove(const xaya::ParsedBoardState &state,
                              xaya::BoardMove &mv)
  {
    const auto &chessState = dynamic_cast<const ChessBoardState &>(state);

    proto::BoardMove mvPb;
    if (!InternalAutoMove(chessState, mvPb))
      return false;

    CHECK(mvPb.SerializeToString(&mv));
    return true;
  }

  void
  ChessChannel::MaybeOnChainMove(const xaya::ParsedBoardState &state,
                                 xaya::MoveSender &sender)
  {
    const auto &chessState = dynamic_cast<const ChessBoardState &>(state);
    const auto &id = chessState.GetChannelId();
    const auto &meta = chessState.GetMetadata();

    if (chessState.GetPhase() != ChessBoardState::Phase::FINISHED)
      return;

    const auto &statePb = chessState.GetState();
    CHECK(statePb.has_winner());

    const int loser = 1 - statePb.winner();
    CHECK_GE(loser, 0);
    CHECK_LT(loser, meta.participants_size());
    if (meta.participants(loser).name() != playerName)
      return;

    if (!txidClose.IsNull() && sender.IsPending(txidClose))
    {
      /* If we already have a pending close move, then we are not sending
         another.  Note that there is a slight chance that this has weird
         behaviour with reorgs:  Namely if the original join gets reorged
         and another second player joins, it could happen that our pending
         close is invalid (because it was signed by the previous opponent).

         But this is very unlikely to happen.  And even if it does, there
         is not much harm.  The worst that can happen is that we wait for
         the current move to be confirmed, and then send a correct new one.  */

      LOG(INFO)
          << "We already have a pending channel close: "
          << txidClose.ToHex();
      return;
    }

    Json::Value data(Json::objectValue);
    data["id"] = id.ToHex();
    data["r"] = xaya::EncodeBase64(meta.reinit());

    Json::Value mv(Json::objectValue);
    mv["l"] = data;
    txidClose = sender.SendMove(mv);
    LOG(INFO)
        << "Channel has a winner and we lost, closing on-chain: "
        << txidClose.ToHex();
  }

} // namespace chess
