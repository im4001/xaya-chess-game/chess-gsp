#pragma once

// database schemas goes here

#include <xayagame/sqlitestorage.hpp>

namespace chess
{
  void SetupChessSchema(xaya::SQLiteDatabase &db);
}