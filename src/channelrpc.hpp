// Copyright (C) 2019 The Xaya developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#pragma once

#include "channel.hpp"
#include "proto/boardmove.pb.h"
#include "rpc-stubs/chesschannelrpcserverstub.h"

#include <gamechannel/daemon.hpp>

#include <json/json.h>
#include <jsonrpccpp/server.h>

#include <string>

namespace chess
{

  /**
   * RPC server for the ships channel daemon interface.
   */
  class ChessChannelRpcServer : public ChessChannelRpcServerStub
  {

  private:
    /** The ships channel data for RPC processing.  */
    ChessChannel &channel;

    /**
     * The ChannelDaemon instance to use for RPC processing.  This by itself
     * also exposes the underlying ChannelManager.
     */
    xaya::ChannelDaemon &daemon;

    /**
     * Processes a local move given as proto.  When this method gets called,
     * we already hold the lock on the channel manager, and pass the instance
     * in directly.
     */
    static void ProcessLocalMove(xaya::ChannelManager &cm,
                                 const proto::BoardMove &mv);

  public:
    explicit ChessChannelRpcServer(ChessChannel &c, xaya::ChannelDaemon &d,
                                   jsonrpc::AbstractServerConnector &conn)
        : ChessChannelRpcServerStub(conn), channel(c), daemon(d)
    {
    }

    void stop() override;
    Json::Value getcurrentstate() override;
    Json::Value waitforchange(int knownVersion) override;

    void setposition(const std::string &str) override;
    // void revealposition() override;
    std::string filedispute() override;

    bool validateposition(const std::string &str) override;
  };

} // namespace chess

